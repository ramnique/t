<?php

class m130622_031711_create_tweets_table extends CDbMigration
{
	public function up()
	{
        $this->createTable('tweets', array(
			'id'        => 'pk',
			'tweet_id'  => 'string NOT NULL',
			'user_id'   => 'string NOT NULL',
			'user_name' => 'string NOT NULL',
			'time'      => 'datetime NOT NULL',
			'content'   => 'text NOT NULL',
			'urls'      => 'text',
        ));

        $this->createIndex('tweets_tweet_id_unique', 'tweets', 'tweet_id', TRUE);
	}

	public function down()
	{
        $this->dropTable('tweets');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
