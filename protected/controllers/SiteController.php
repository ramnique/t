<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

    public function actionGetStats() {
        $response = array();

        // things to report
        $all_urls   = array();
        $usr_urls   = array();
		$all_tweets = array();
		$domains    = array();

        // LOOP
        $tweets = Tweet::model()->findAll();
        foreach ($tweets as $tweet) {
            $urls = json_decode($tweet->urls);

            // collect tweet
            $all_tweets[$tweet->tweet_id] = $tweet->attributes;

            // collect urls
            foreach ($urls as $url) {
                if (! in_array($url->expanded_url, $all_urls)) {
                    $all_urls[] = $url->expanded_url;
                }
            }

            // collect user urls
            if (! isset($usr_urls[$tweet->user_name]))
                $usr_urls[$tweet->user_name] = array();

            if (! in_array($url->expanded_url, $usr_urls[$tweet->user_name])) {
                $usr_urls[$tweet->user_name][] = $url->expanded_url;
            }
        }

        // collect domain counts
        foreach ($all_urls as $url) {
        	$parsed = parse_url($url);
        	$domain = $parsed['host'];

            if (! isset($domains[$domain]))
                $domains[$domain] = 0;

            $domains[$domain]++;
        }

		$response['all_urls']   = $all_urls;
		$response['usr_urls']   = $usr_urls;
		$response['all_tweets'] = $all_tweets;
		$response['domains']    = $domains;

        echo json_encode($response);
    }

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		$this->render('//twtest/process');
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}
}
