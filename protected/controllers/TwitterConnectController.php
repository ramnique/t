<?php

class TwitterConnectController extends Controller
{
    /**
     * Declares class-based actions.
     */
    public function actions()
    {
        return array();
    }

    public function actionIndex()
    {
        $twitter = Yii::app()->twitter->getTwitter();
        $request_token = $twitter->getRequestToken();

        Yii::app()->session['oauth_token'] =
            $token = $request_token['oauth_token'];
        Yii::app()->session['oauth_token_secret'] =
            $request_token['oauth_token_secret'];

        if($twitter->http_code == 200){
            $url = $twitter->getAuthorizeURL($token);
            // send request
            $this->redirect($url);
        }else{
            $this->redirect(Yii::app()->homeUrl);
        }
    }

    public function actionCallback() {
        $response = array(
            'success' => false,
        );

        /* If the oauth_token is old redirect to the connect page. */
        if (isset($_REQUEST['oauth_token']) &&
            Yii::app()->session['oauth_token'] !== $_REQUEST['oauth_token']) {
            Yii::app()->session['oauth_status'] = 'oldtoken';
        }

        /* Create TwitteroAuth object with app key/secret and token key/secret from default phase */
        $twitter = Yii::app()->twitter->getTwitterTokened(Yii::app()->session['oauth_token'], Yii::app()->session['oauth_token_secret']);

        /* Request access tokens from twitter */
        $access_token = $twitter->getAccessToken($_REQUEST['oauth_verifier']);

        /* Save the access tokens. Normally these would be saved in a database for future use. */
        Yii::app()->session['access_token'] = $access_token;

        /* Remove no longer needed request tokens */
        unset(Yii::app()->session['oauth_token']);
        unset(Yii::app()->session['oauth_token_secret']);

        if (200 == $twitter->http_code) {
            /* The user has been verified and the access tokens can be saved for future use */
            Yii::app()->session['status'] = 'verified';

            $response['success'] = true;
        }

        $this->render('//twtest/oauth_response', array(
            'response' => $response,
        ));
    }

    public function actionGetUserList() {
        $response = array();

        if (Yii::app()->session['status'] == 'verified' &&
            Yii::app()->session['access_token']) {
            $access_token = Yii::app()->session['access_token'];

            // get twitter object
            $twitter = Yii::app()->twitter->getTwitterTokened($access_token['oauth_token'],$access_token['oauth_token_secret']);

            // get user data, friends, followers
            $twuser= $twitter->get("account/verify_credentials");
            $friends = $twitter->get('friends/list');
            $followers = $twitter->get('followers/list');

            // build an array of twitter usernames
            $data = array( $twuser->screen_name );

            foreach ($friends->users as $f) {
                $data[] = $f->screen_name;
            }

            foreach ($followers->users as $f) {
                $data[] = $f->screen_name;
            }

            // remove duplicates
            $response = array_keys(array_flip($data));
        }

        echo json_encode($response);
    }

    public function actionGetTweets() {
        $response = array(
            'num'   => 0,
        );

        // get twitter username
        $handle = trim($_POST['handle']);

        if (Yii::app()->session['status'] == 'verified' &&
            Yii::app()->session['access_token'] && $handle) {
            $access_token = Yii::app()->session['access_token'];

            // get twitter object
            $twitter = Yii::app()->twitter->getTwitterTokened($access_token['oauth_token'],$access_token['oauth_token_secret']);

            // base search url
            $url = 'search/tweets';

            // build criteria
            $terms = array();
            $terms[] = 'from:'.$handle;
            $date = date('Y-m-d', strtotime('-5 days'));
            $terms[] = "since:".$date;
            $terms[] = "filter:links";

            // search
            $tweets = $twitter->get($url, array(
                'q' => rawurldecode(implode(' ', $terms)),
                'include_entities' => 'true',
            ));

            // populate db
            $count = 0;
            foreach ($tweets->statuses as $t) {
                // store only if urls present
                if (sizeof($t->entities->urls)) {
                    $count++;

                    // create object
                    $newTweet            = new Tweet();
                    $newTweet->tweet_id  = $t->id_str;
                    $newTweet->time      = date('Y-m-d H:i:s', strtotime($t->created_at));
                    $newTweet->user_id   = $t->user->id_str;
                    $newTweet->user_name = $t->user->screen_name;
                    $newTweet->content   = $t->text;
                    $newTweet->urls      = json_encode($t->entities->urls);

                    // make sure it does not exist
                    $existing = Tweet::model()->find(array(
                        'condition' => 'tweet_id=:tweet_id',
                        'params'    => array(':tweet_id' => $newTweet->tweet_id),
                    ));

                    if (! $existing)
                        $newTweet->save();
                }
            }

            // send back no. of tweets found
            $response['num'] = $count;
        }

        echo json_encode($response);
    }
}
