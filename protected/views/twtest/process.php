<?php /* @var $this Controller */
$this->layout = 'primary';
?>

<style type="text/css">
    .tab-results {
        display: none;

        .dtables {
            display: none;
        }
    }

    .tab-processing {
        display: none;
    }

    .tab-start {
        .alert {
            display: none;
        }
    }

    .results-button {
        display: none;
    }
</style>

<div class="tab-start">
    <span class="alert"></span>

    <a href="javascript:void(0)" class="btn btn-primary connect-button btn-large"><i class="icon-random icon-white"></i> Connect with twitter</a>
</div>

<div class="tab-processing">
    <div class="row-fluid">
        <div class="span4">
            <h3 class="state">Getting tweets &hellip;</h3>
        </div>
        <div class="span2">
            <a href="#" class="results-button btn btn-large btn-danger">View stats</a>
        </div>
    </div>

    <div class="row-fluid">
        <div class="progress-bar progress progress-striped active">
          <div class="bar" style="width: 0;"></div>
        </div>
    </div>

    <div class="row-fluid">
        <table class="table table-striped user-table"></table>
    </div>
</div>

<div class="tab-results">
    <div class="row-fluid">
        <div class="span4">
            <h3 class="state"></h3>
        </div>
    </div>

    <div class="row-fluid">
        <div class="span6">
            <h4>Tweets</h4>

            <table class='dtables t-tweets'>
                <thead>
                    <tr>
                        <th>user</th>
                        <th>tweet</th>
                        <th>time</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>

        <div class="span6">
            <h4>User url count</h4>

            <table class='dtables t-user-urls'>
                <thead>
                    <tr>
                        <th>user</th>
                        <th>url count</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>

    <hr />
    <div class="row-fluid">
        <div class="span6">
            <h4>Domain counts</h4>

            <table class='dtables t-domain-urls'>
                <thead>
                    <tr>
                        <th>domain</th>
                        <th>url count</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>

        <div class="span6">
            <h4>All urls</h4>

            <table class='dtables t-urls'>
                <thead>
                    <tr>
                        <th>url</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>

<script type="text/javascript">
(function () {
    var app = {
        // oauth module
        oauth: {
            timeout: null,

            state: {
                busy: function () {
                    $(".connect-button").hide();
                    $(".tab-start .alert")
                        .html("Working &hellip;")
                        .removeClass("alert-error")
                        .addClass("alert-warning")
                        .show()
                },

                error: function () {
                    $(".connect-button").show();
                    $(".tab-start .alert")
                        .html("There was an error. Please try again &hellip;")
                        .removeClass("alert-warning")
                        .addClass("alert-error")
                        .show()
                }
            },

            monitorOauthWindow: function (w) {
                app.oauth.state.busy();

                if (w !== null) {
                    // Keep checking oauth window to see if it has been closed
                    // in error
                    this.timeout = setInterval(function () {

                        if (w.closed) {
                            app.oauth.state.error();
                        }

                    }, 500);
                } else {
                    // popup blocker?
                    app.oauth.state.error();
                }

            },

            start: function (provider) {
                var w, woptions = "width=500,height=450,location=no,scrollbars=no,menubar=no,resizable=no,status=no";

                w = window.open("<?= $this->createUrl('twitterConnect/index') ?>", "", woptions);

                app.oauth.monitorOauthWindow(w);

            },

            init: function () {
                $(".connect-button").click(function () {
                    app.oauth.start();
                });
            }
        },

        // Tweet collection module
        processor: {
            users           : [],
            completed_users : [],
            totalcount      : 0,

            list_users: function () {
                var html = "", i;

                for (i = 0; i < app.processor.users.length; i++) {
                    html = html + "<tr data-name='" + app.processor.users[i] + "'>" +
                        "<td>" + app.processor.users[i] + "</td>" +
                        "<td><span class='label'>waiting</span></td>" +
                        "</tr>";
                }

                $(".user-table").html(html);
            },

            progress_refresh: function () {
                var percent = (app.processor.completed_users.length / app.processor.users.length) * 100;
                $(".tab-processing .progress-bar .bar").css("width", percent + "%");

                if (app.processor.completed_users.length == app.processor.users.length) {
                    $(".progress-bar").removeClass("active")
                        .find(".bar").addClass("bar-success");

                    $(".tab-processing h3.state").html("Fetched " + app.processor.totalcount + " tweets from " + app.processor.users.length + " users.");

                    $(".results-button").show();
                }
            },

            getUserTweets: function (username, callback) {
                // mark user as processing
                $(".user-table tr[data-name='" + username + "'] .label")
                    .addClass("label-success")
                    .html("fetching &hellip;");

                $.ajax({
                    type: "POST",
                    url: "<?= $this->createUrl('twitterConnect/getTweets') ?>",
                    data: {
                        'handle': username
                    },
                    dataType: 'json',
                    success: function (data) {
                        // update total count
                        app.processor.totalcount = app.processor.totalcount + data.num;

                        // mark user as done
                        $(".user-table tr[data-name='" + username + "'] .label")
                            .removeClass("label-success")
                            .addClass("label-info")
                            .html(data.num + " tweets");

                        if (callback !== undefined) {
                            callback(data);
                        }
                    }
                });
            },

            loadUserList: function (callback) {
                $(".tab-processing h3.state").html("Fetching user list &hellip;");
                $.ajax({
                    type: "POST",
                    url: "<?= $this->createUrl('twitterConnect/getUserList') ?>",
                    dataType: 'json',
                    success: function (data) {
                        $(".tab-processing h3.state").html("Getting tweets from " + data.length + " users &hellip;");

                        if (callback !== undefined) {
                            callback(data);
                        }
                    }
                });
            },

            composer: function () {
                var i;

                for (i = 0; i < app.processor.users.length; i++) {
                    if ($.inArray(app.processor.users[i], app.processor.completed_users) === -1) {
                        app.processor.getUserTweets(app.processor.users[i], function () {
                            app.processor.completed_users.push(app.processor.users[i]);
                            app.processor.progress_refresh();
                            app.processor.composer();
                        });

                        return false;
                    }
                }
            },

            start: function () {
                $(".tab-start").hide();
                $(".tab-processing").show();

                app.processor.loadUserList(function (data) {
                    app.processor.users = data;
                    app.processor.list_users();
                    app.processor.composer();
                });
            },

            init: function () {
                //
            }
        },

        // results module
        results: {
            populate_tweets: function (data) {
                var html = "";

                $.each(data, function(id, tweet) {
                    html = html + "<tr>" +
                        "<td>" + tweet.user_name + "</td>" +
                        "<td>" + tweet.content + "</td>" +
                        "<td>" + tweet.time + "</td>" +
                        "</tr>";
                });

                $(".tab-results .t-tweets tbody").html(html);
            },

            populate_user_url_counts: function (data) {
                var html = "";

                $.each(data, function(username, tweets) {
                    html = html + "<tr>" +
                        "<td>" + username + "</td>" +
                        "<td>" + tweets.length + "</td>" +
                        "</tr>";
                });

                $(".tab-results .t-user-urls tbody").html(html);
            },

            populate_domains: function (data) {
                var html = "";

                $.each(data, function(domain, count) {
                    html = html + "<tr>" +
                        "<td>" + domain + "</td>" +
                        "<td>" + count + "</td>" +
                        "</tr>";
                });

                $(".tab-results .t-domain-urls tbody").html(html);
            },

            populate_all_urls: function (data) {
                var html = "";

                $.each(data, function(index, domain) {
                    html = html + "<tr>" +
                        "<td>" + domain + "</td>" +
                        "</tr>";
                });

                $(".tab-results .t-urls tbody").html(html);
            },

            activateDataTables: function () {
                $(".dtables").dataTable();
            },

            getStats: function (callback) {
                $(".tab-results h3.state").html("Fetching stats &hellip;");

                $.ajax({
                    type: "GET",
                    url: "<?= $this->createUrl('site/getStats') ?>",
                    dataType: 'json',
                    success: function (data) {
                        $(".tab-results h3.state").hide();

                        if (callback !== undefined) {
                            callback(data);
                        }
                    }
                });
            },

            init: function () {
                $(".results-button").click(function () {
                    $(".tab-processing").hide();
                    $(".tab-results").show();
                    app.results.getStats(function (data) {
                        app.results.populate_tweets(data.all_tweets);
                        app.results.populate_user_url_counts(data.usr_urls);
                        app.results.populate_domains(data.domains);
                        app.results.populate_all_urls(data.all_urls);
                        app.results.activateDataTables();
                        $(".dtables").show();
                    });
                });
            }
        },

        init: function () {
            app.oauth.init();
            app.processor.init();
            app.results.init();
        }
    };

    window.app = app;
}());

app.init();

function oauth_login_success() {
    app.processor.start();
}
function oauth_login_fail() {
    app.oauth.state.error();
}
</script>
