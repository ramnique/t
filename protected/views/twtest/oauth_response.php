<?php /* @var $this Controller */
$this->layout = 'primary';
?>
<script>
var response = <?= json_encode($response) ?>;

if (response.success == true) {
    if (typeof window.opener.oauth_login_success !== "undefined") {
        window.opener.oauth_login_success();
    }
    self.close();
} else {
    if (typeof window.opener.oauth_login_success !== "undefined") {
        window.opener.oauth_login_fail();
    }
    self.close();
}
</script>

